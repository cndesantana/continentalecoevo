# version 0.1
#= ContinentalEcoEvo

# Speciation events of clownfish in Coral reefs

De Santana, Melian, Pellissier (2016)

To Call: ./coralreef SEED NREAL G J FILE1 FILE2 FILE3

SEED  - (Random seed)

NREAL - (Number of realizations)

G     - (Number of generations per landscape)

J     - (Number of individuos per available site)

FILE1 - (Geographic Locations)

FILE2 - (File containing the names of the files of the Networks of Connections between sites)

FILE3 - (File containing the names of the files of the Matrices of dendritic distances between sites)

That means that each line of FILE2 and FILE3 represent one slice of the Dynamic-Landscape

Dynamic Landscape ------> t0 -> t0.5 -> t1.0 -> t1.5 -> ... -> tn

---------------------------------------------------------------

# step-by-step of the model

1) To perform the demographic dynamic during G/2 generations

2) To Compare matrix of distances in time t.0 and in time t.5
   a - For each site that disapear from t.0 to t.5, all individuals of this site will migrate to other sites in the same cluster/module/component (CONTRACTION event). The number of individuals that will migrate to each site will be proportional to the distance between these pair of sites. Probability of prevalence of the migrant individual (pp) is 50% (it means the prevalence is neutral)

   b - The model can be easily adapted to change the probability of prevalence: for niche prevalence, pp < 50%; for migrant prevalence, pp > 50%. 

3) To perform the demographic dynamic during other G/2 generations

4) To Compare matrix of distances in time t.5 and in time t+1.0
   a - For each site that apear from t.5 to t+1.0, individuals will migrate from all other sites in the same cluster/module/component (EXPANSION event). The number of individuals that will migrate to each site will be proportional to the distance between the source site and the target site. Probability of prevalence of the migrant individual (pp) is 100%. 

5) Come back to 1)

=#


module continentalecoevo

function readLocations(filename::ASCIIString)
	dat = readdlm(filename,' ');
	w = hcat(dat[:,1],dat[:,2]);
	w;
end

#For each site that disapear from t.0 to t.5, all individuals of this site will migrate to other sites in the 
#same cluster/module/component of the landscape network, according to the distance between these pair of sites and 
#with a probability of prevalence of 50% (niche, neutral or migrant). (CONTRACTION)
function contraction!(D1,D2,Di1,Dc1,J::Int64,R,MA,anaG::Int64,ts::Int64,MC,lastspecies::Int64,listofanagenesis,phylogenyfile,ri,typeofinput);#
	if(typeofinput == 1)
	   sites1=find(diag(D1).==1);
	   sites2=find(diag(D2).==1);
        elseif(typeofinput ==2)
           sites1=unique(D1.rowval);
           sites2=unique(D2.rowval);
        end

	contractedsites = setdiff(sites1,sites2);
	for(c in 1:length(contractedsites))#for each site that disapears from the landscape
		source = contractedsites[c];#this site is the source of individuals that will emmigrate to other sites
		source_neigh = find(Di1[source,:].>0);#sites reached by source at time step to (Di1)
		target_sites = intersect(source_neigh,sites2);#neighbor sites at time step t0 that remain in the landscape at time step t0.5 (intersect)
	#To migrate from source to target_sites	
		if(length(target_sites) > 0)
			for (ji in 1:J)
				target,R,MigrantSpecies = EmmigrationEvent(R,source,target_sites,Dc1,J,ji);
				MA = UAM(MA,R,target,source,MigrantSpecies,anaG,ts);#Update Anagenesis after Migration
			end
		end#if length(target_sites > 0)
		R[source,:]=0;#"killing" all the individuals of site source
	end	
	MA,R,lastspecies,listofanagenesis = checkAna(MA,R,anaG,lastspecies,listofanagenesis,ts,phylogenyfile,ri); #After the update of matrix MA, we check for events of Anagenesis
end

#For each site that apear from t.5 to t+1.0, individuals will migrate from all other sites in the 
#same cluster/module/component of the landscape network, according to the distance to the target site. 
#Probability of prevalence of the migrant individual is 100%. (EXPANSION)
function expansion!(D2,D3,Di2,Dc2,J::Int64,R,MA,anaG::Int64,ts::Int64,MC,lastspecies::Int64,listofanagenesis,phylogenyfile,ri,typeofinput);#
	if(typeofinput == 1)
	   sites2=find(diag(D2).==1);
	   sites3=find(diag(D3).==1);
        elseif(typeofinput ==2)
           sites2=unique(D2.rowval);
           sites3=unique(D3.rowval);
        end

	expandedsites = setdiff(sites3,sites2);
	for(c in 1:length(expandedsites))#for each new site that appears in the landscape
		target = expandedsites[c];#this site is the target of individuals that will immigrate from other sites
		target_neigh = find(Di2[target,:].>0);#sites reached by target at time step t1 (Di3)
		source_sites = intersect(target_neigh,sites3);#neighbor sites at time step t0.5 that remain in the landscape at time step t1 (intersect)
	#To migrate from source_sites to target	
		if(length(source_sites) > 0)
			for (ji in 1:J)
				source,R,MigrantSpecies = ImmigrationEvent(R,source_sites,target,Dc2,J,ji);
				MA = UAM(MA,R,target,source,MigrantSpecies,anaG,ts);#Update Anagenesis after Migration
			end
		else#if length(source_sites == 0)
			R[target,:]=lastspecies + 1;#all the individuals of the new site are from the same species #TOFIX
			lastspecies = lastspecies + 1; 
		end
	end	
	MA,R,lastspecies,listofanagenesis = checkAna(MA,R,anaG,lastspecies,listofanagenesis,ts,phylogenyfile,ri); #After the update of matrix MA, we check for events of Anagenesis

end

function demography!(ts::Int64,S::Int64,J::Int64,D,Dc,mr::Float64,vr::Float64,R,MA,anaG::Int64,MC,lastspecies::Int64,listofanagenesis,phylogenyfile,ri,typeofinput)
	if(typeofinput == 1)
           sites=find(diag(D).==1);
        elseif(typeofinput ==2)
           sites=unique(D.rowval);
        end
	actualS=length(sites);
#        println("Running at $actualS sites")
	for (j = 1:(actualS*J))#For each individual in each site
		ts = ts+1;#Time step increases
       		#Demography - Resources
       		KillHab = sites[rand(1:actualS)];#which site to kill
		KillInd = rand(1:J);#which individual to kill
		mvb = rand();
		MigrantHab = rand()*maximum(Dc[KillHab,:]);
		BirthLocal = rand(1:J);#which individual to born

       		if mvb <= mr;#Migration event
			kr,R,MigrantSpecies = MigrationEvent(R,KillHab,MigrantHab,KillInd,Dc,J,S);
			MA = UAM(MA,R,KillHab,kr,MigrantSpecies,anaG,ts);#Update Anagenesis after Migration

       		elseif (mvb > mr) && (mvb <= mr+vr);#Cladogenesis Speciation event
			if(vr > 0)#we only simulate Cladogenesis when the probability is higher than 0
				MC,R,lastspecies = CladogenesisEvent(MC,R,KillHab,KillInd,ts,lastspecies,phylogenyfile,ri);
				MA = UAS(MA);#Update Anagenesis after Cladogenesis Speciation
			end
       		else #Birth event
			R = BirthEvent(R,BirthLocal,KillInd,KillHab);
			MA = UAB(MA);
       		end;
		MA,R,lastspecies,listofanagenesis = checkAna(MA,R,anaG,lastspecies,listofanagenesis,ts,phylogenyfile,ri); #After the update of matrix MA, we check for events of Anagenesis
	end;#end S*J
end


function createMC(MC,Sti,Sp,ts)
	MC = [Sti Sp 1 ts];
end

function updateMC(MC,Sti,Sp,event,ts)
	pos = find( (MC[:,1].==Sti) & (MC[:,2].==Sp))#position in the matrix MC referred to the presence of individuals of species 'Sp' in site 'Sti'
	if length(pos)!=0
		MC[pos,3] = event;#event: -1 for extinction; 1 for speciation
		return MC;
	else
		MC = cat(1,MC,[Sti Sp 1 ts]);
		return MC;
	end
end

function checkIfThereIsMC(MC,Sti,Sp,ts)
	if length(MC)==0
		MC = createMC(MC,Sti,Sp,ts);
		return MC;
	else
		pos = find( (MC[:,1].==Sti) & (MC[:,2].==Sp))#position in the matrix MA referred to the presence of individuals of species 'Sp' coming from site 'Stj' to site 'Sti'
		if length(pos)==0
			MC = cat(1,MC,[Sti Sp 1 ts]);
			return MC;
		end
	end
	return MC;
end


function createMA(MA,Sti,Stj,Sp,ts)
	MA = [Sti Stj Sp 1 ts];
end


function updateMA(MA)
	if length(MA)>0
		MA[:,4]= MA[:,4] + 1;
	end
	return MA;
end


function UAM(MA,R,Sti,Stj,Sp,anaG,ts)
	MA = updateMA(MA);

	if length(MA)==0 #Si no hay MA
		MA = createMA(MA,Sti,Stj,Sp,ts);#Crea MA
		return MA;
	else#Si hay MA
		pos = find( (MA[:,1].==Sti) & (MA[:,2].==Stj) & (MA[:,3].==Sp))#position in the matrix MA referred to the presence of individuals of species 'Sp' coming from site 'Stj' to site 'Sti'
		if length(pos)==0 #No hay la linea
			indalive = length(find(R[Sti,:].==Sp))#Hay individuos de la specie sp vivos en el sitio Sti
			if (indalive == 0) #Checking if there are individuals of species 'Sp' alive in site 'Sti'
				MA = cat(1,MA,[Sti Stj Sp 1 ts]);#Crea la linea, empiezando con 1
				return MA;
			end
			return MA;
		else #Ya hay la linea
#			MA = MA[:,1:size(MA,2).!=pos];#Borra la columna 'pos' de la matriz MA!!
			MA = MA[1:size(MA,1).!=pos,:];#Borra la linea 'pos' de la matriz MA!!
			return MA;
		end
	end

	MA;
end


function UAS(MA)
	MA = updateMA(MA);
end


function UAB(MA)
	MA = updateMA(MA);
end


function checkAna(MA,R,anaG,lastspecies,listofanagenesis,ts,phylogenyfile,ri)
	if length(MA)>0
		pos = find(MA[:,4] .>= anaG)
		for (a in 1:length(pos))
			Sti=MA[pos[a],1];#pos represents the lines of the matrix. pos[a] is one line. MA[pos[a],1] is the a-th target site
			Stj=MA[pos[a],2];#pos represents the lines of the matrix. pos[a] is one line. MA[pos[a],3] is the a-th source site 
			Sp=MA[pos[a],3];#pos represents the lines of the matrix. pos[a] is one line. MA[pos[a],3] is the a-th species 
			MA, R, lastspecies,listofanagenesis = AnagenesisSpeciation(MA,R,Sti,Stj,Sp,lastspecies,listofanagenesis,ts,phylogenyfile,ri);#speciation in target site
		end
	end
	return MA,R,lastspecies,listofanagenesis;
end

function printPhylogeny(new,old,ts,phylogenyfile,ri)
	writedlm(phylogenyfile,[ri old new ts],' ');
        flush(phylogenyfile); 
end

function AnagenesisSpeciation(MA,R,Sti,Stj,Sp,lastspecies,listofanagenesis,ts,phylogenyfile,ri)
	newspeciesAna = lastspecies + 1;#the id of the new species
	oldindividuals = find( R[Sti,:].==Sp )#the position of all the individuals of the 'old' species 'Sp' in the target site 
	R[Sti,oldindividuals] = newspeciesAna;#the speciation itself: all the individuals of former species 'Sp' in the target site are now from a new species 'newspeciesAna'
	printPhylogeny(newspeciesAna,Sp,ts,phylogenyfile,ri);
 
	pos = find( (MA[:,1].==Sti) & (MA[:,3].==Sp))#position in the matrix MA referred to the presence of individuals of species 'Sp' in site 'Sti' 
	MA = MA[1:size(MA,1).!=pos,:];#Borra la linea 'pos' de la matriz MA!!

	if length(listofanagenesis)>0
		listofanagenesis = cat(1,listofanagenesis,[Sti lastspecies]);
		return MA,R,newspeciesAna,listofanagenesis;
	else
		listofanagenesis = cat(1,[Sti lastspecies]);	
		return MA,R,newspeciesAna,listofanagenesis;
	end
end

function CladogenesisEvent(MC,R,Sti,Individual,ts,lastspecies,phylogenyfile,ri)
	newspeciesClado = lastspecies + 1;
	printPhylogeny(newspeciesClado,R[Sti,Individual],ts,phylogenyfile,ri);
   	R[Sti,Individual] = newspeciesClado;
	MC = checkIfThereIsMC(MC,Sti,newspeciesClado,ts);                                    


	return MC,R,newspeciesClado; 
end

function EmmigrationEvent(R,source,target_sites,Dc,J,MigrantInd)
       	MigrantSpecies = R[source,MigrantInd];

       	NicheInd = rand(1:J);
	MigrantHab = rand()*maximum(Dc[source,target_sites]);
	target=target_sites[findfirst(Dc[source,target_sites].>MigrantHab)];
       	NicheSpecies = R[target,NicheInd];

	probsurvive=rand();
	if(probsurvive > 0.5)
         	R[target,NicheInd] = R[source,MigrantInd];#neutral probability to survive the migrant or the niche species
	end

	target,R,R[target,NicheInd];	
end;


function ImmigrationEvent(R,source_sites,target,Dc,J,NicheInd)

       	MigrantInd = rand(1:J);
	MigrantHab = rand()*maximum(Dc[source_sites,target]);
	source=source_sites[findfirst(Dc[source_sites,target].>=MigrantHab)];

 	R[target,NicheInd] = R[source,MigrantInd];#target site receives one individual from another site (source)

	source,R,R[target,NicheInd];	
end;


function MigrationEvent(R,KillHab,MigrantHab,KillInd,Dc,J,S)
	target = KillHab;
	source=findfirst(Dc[target,:].>=MigrantHab);#All the sites at a distance lower than the threshold 'MigrantHab'
	MigrantInd = rand(1:J);
	R[target,KillInd] = R[source,MigrantInd];

	return source,R,R[target,KillInd];
end;


function BirthEvent(R,BirthLocal,KillInd,KillHab)
	R[KillHab,KillInd] = R[KillHab,BirthLocal];
	return R;
end

function richnessanalysis!(S,R)	
	richnessspeciesR = [];
	alfarichness = zeros(S);
	#%gamma richness
	for (i in 1:S)
		AR = sort(R'[:,i]);
		richnessspeciesR = [richnessspeciesR; unique(AR)];
		alfarichness[i] = length(find(unique(AR)));#number of elements different from zero (species different from species '0')
	end;
	gamma = length(unique(sort(richnessspeciesR)));
	
	return gamma,alfarichness;	
end

function calculateSpeciationMA(listofanagenesis,R,S)
	speciatedMA = round(Int64,zeros(S));
	for i in 1:S
		speciesR = unique(sort(R'[:,i]))';
		pos = find(listofanagenesis[:,1].== i);
		speciesMA = unique(sort(listofanagenesis[pos]));
		speciatedMA[i] = length(setdiff(speciesMA,setdiff(speciesMA,speciesR))');
	end
	return speciatedMA;
end

function calculateSpeciationMC(MC,R,S)
	speciatedMC = round(Int64,zeros(S));
	for i in 1:S
		speciesR = unique(sort(R'[:,i]))';
		pos = find(MC[:,1].== i);
		speciesMC = unique(sort(MC[pos]));
		speciatedMC[i] = length(setdiff(speciesMC,setdiff(speciesMC,speciesR))');
	end
	return speciatedMC;
end

function readDistanceMatrix2(filename,S)
   dat = readdlm(filename,' ');
   mat = sparse(round(Int,dat[:,1]), round(Int,dat[:,2]), dat[:,3], S, S);
   return(mat) 
end

function readDistanceMatrix1(filename,S)
   mat = readdlm(filename,' ');
   return(mat);
end


function dynamic(seed::Int64,nreal::Int64,G::Int64,J::Int64,vr::Float64,mr::Float64,anaG::Int64,filename1::ASCIIString,filename2::ASCIIString,filename3::ASCIIString)

	lastspecies = 1;

	locations = readLocations(filename1);#the location of the points of the landscape.
	networks=open(filename2,"r");
	listofnet = readdlm(networks,'\n');#list of files that contain the connectivity of sites at each landscape-time-step
	close(networks);
	outputfilename = string("RichnessPerSite_AnaG_",anaG,"_MR_",signif(mr,3),"_VR_",signif(vr,3),".txt")	
	phylogenyfilename = string("Phylogeny_AnaG_",anaG,"_MR_",signif(mr,3),"_VR_",signif(vr,3),".txt");	

	if(isfile(outputfilename)==false)
        	outputfile = open(outputfilename,"w")	
        	writedlm(outputfile,["Repl LTS GEN Site X Y gamma alpharich SpecANA SpecCLA DispersalRich"]); 
                flush(outputfile)
        	close(outputfile);
        end

	if(isfile(phylogenyfilename)==false)
		phylogenyfile = open(phylogenyfilename,"w")	
		writedlm(phylogenyfile,["Repl Ancestral Derived Age"]); 
		close(phylogenyfile);
	end

	S = size(locations,1);#Total Number of sites in the landscape
	landscapetimesteps = length(listofnet);#

	outputfile = open(outputfilename,"a")	
	phylogenyfile = open(phylogenyfilename,"a")	
	for (ri in 1:nreal)#realizations
		srand(seed+(7*ri));
		r_randomdynamics = zeros(G);#store one value of r for each generation
		R = round(Int64,zeros(S,J));#List of individuals in each site
		MA = [];#Matrix to calculate Anagenesis speciation (MA = [Sti, Stj, S, C]):  Sti - Target site; Stj - Source site; S - Species; C - Counter time
		MC = [];#Matrix to control Cladogenesis speciation (MA = [Sti, Stj, S, C]):  Sti - Target site; Stj - Source site; S - Species; C - Counter time
		listofanagenesis = [];#List of Anagenesis events
		ts=0;
		lts=1;#landscape-time-step counter	
		networkfile=listofnet[lts];
                typeofinput = 2;
                if(split(networkfile,'_')[1] == "matrix")
                   typeofinput = 1; 
                end
                println(split(networkfile,'_')[1]);
                D3 = [];	
		if(typeofinput == 1)
                   D3 = readDistanceMatrix1(networkfile,S);
		elseif(typeofinput == 2)
                   D3 = readDistanceMatrix2(networkfile,S);
                end
		Dc3 = copy(D3);
		Di3 = copy(D3);
		cumsum!(Dc3,Di3);
         	if(typeofinput == 1)
		   initialsites = find(diag(D3).==1);
                   ninitialsites = length(initialsites);
                elseif(typeofinput == 2)
                   initialsites=unique(D3.rowval);
                   ninitialsites = length(initialsites);
                end

		R[initialsites,:] = 1;
                logfile = open("./logfile","a")
		while (lts < landscapetimesteps)
                        println("landscape $(lts) = $(listofnet[lts])");
                        if(typeofinput == 2)
                           time_t = parse(Int,split(split(listofnet[lts],'_')[5],'.')[1]);
                           time_t1 = parse(Int,split(split(listofnet[lts+2],'_')[5],'.')[1]);
                        elseif(typeofinput == 1)
                           time_t = parse(Int,split(split(listofnet[lts],'_')[3],'.')[1]);
                           time_t1 = parse(Int,split(split(listofnet[lts+2],'_')[3],'.')[1]);
                        end
                        deltat = time_t1 - time_t;
                        endtime1 = round(deltat*(G/2));
			D1 = copy(D3);
                        Di1 = copy(Di3);
                        Dc1 = copy(Dc3);
			for (k = 1:endtime1)
			        writedlm(logfile,[lts k],' ');
                                flush(logfile) 
				demography!(round(Int64,lts*(G-1)+k),S,J,D1,Dc1,mr,vr,R,MA,anaG,MC,lastspecies,listofanagenesis,phylogenyfile,ri,typeofinput);
			end;#end 1:G/2
#;#Intermediate landscape
			lts=lts+1;
                        if(lts <= landscapetimesteps)
		        	networkfile=listofnet[lts];
                                println("landscape $(lts) = $(listofnet[lts])");
                                D2 = [];	
        	        	if(typeofinput == 1)
                                   D2 = readDistanceMatrix1(networkfile,S);
        	        	elseif(typeofinput == 2)
                                   D2 = readDistanceMatrix2(networkfile,S);
                                end
		                Di2 = copy(D2);
		                Dc2 = copy(D2);
		                cumsum!(Dc2,Di2);
		        	contraction!(D1,D2,Di1,Dc1,J,R,MA,anaG,round(Int64,lts*(G/2)),MC,lastspecies,listofanagenesis,phylogenyfile,ri,typeofinput);#
                                endtime2 = round(deltat*G);
		        	for (k = (endtime1+1):endtime2)
		        	        writedlm(logfile,[lts k],' ');
                                        flush(logfile) 
		        		demography!(round(Int64,lts*(G-1)+k),S,J,D2,Dc2,mr,vr,R,MA,anaG,MC,lastspecies,listofanagenesis,phylogenyfile,ri,typeofinput);
		        	end;#end (G/2+1):G
			        lts=lts+1;
#;#Final landscape
                                if(lts <= landscapetimesteps)
		                	networkfile = listofnet[lts];
                                        println("landscape $(lts) = $(listofnet[lts])");
                                        D3 = [];	
        	                	if(typeofinput == 1)
                                           D3 = readDistanceMatrix1(networkfile,S);
        	                	elseif(typeofinput == 2)
                                           D3 = readDistanceMatrix2(networkfile,S);
                                        end
        	                	Dc3 = copy(D3);
		                        Di3 = copy(D3);
		                	cumsum!(Dc3,Di3);
		                	expansion!(D2,D3,Di2,Dc2,J,R,MA,anaG,round(Int64,lts*(G)),MC,lastspecies,listofanagenesis,phylogenyfile,ri,typeofinput);#
                                 end#;#End of If for Final landscape
                         end#;#End of If for intermediate landscape
		end#;#end-while 
		#To check the resulting richness
		gamma, alpharich = richnessanalysis!(S,R);
		SpecANA = calculateSpeciationMA(listofanagenesis,R,S);
		SpecCLA = calculateSpeciationMC(MC,R,S);
		DispersalRich = alpharich - (SpecANA + SpecCLA);
		lts=lts+1;
		if(lts >= (landscapetimesteps))
			for i in 1:S
				writedlm(outputfile,[ri lts 1 i locations[i,1] locations[i,2] gamma alpharich[i] SpecANA[i] SpecCLA[i] DispersalRich[i]],' ');
                                flush(outputfile); 
			end
		end
	end#%ri
	close(outputfile);
	close(phylogenyfile);
end#dynamic function

end #module

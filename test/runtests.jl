using continentalecoevo
using Base.Test

## write your own tests here
function main()
	seed = 1;#seed for random numbers (to control the outputs)
	nreal = 1;#Number of realizations
	G = 10;#Number of generations per landscape-time-step
	J = 10;#Number of individuos per site
	filename1 = "./locations2deg.txt";#Name of the file that contains the location of the points with clownfishes.
	filename2 = "./networks2deg.txt";#Name of the file that contains the names of the Networks of connections between each pair of sites that define the landscape
	filename3 = "./dendriticdistance2deg.txt";#Name of the file that contains the names of the Networks of dendritic distances

#	filename1 = "./locations15deg.txt";#Name of the file that contains the location of the points with clownfishes.
#	filename2 = "./networks15deg.txt";#Name of the file that contains the names of the Networks of connections between each pair of sites that define the landscape
#	filename3 = "./dendriticdistance15deg.txt";#Name of the file that contains the names of the Networks of dendritic distances


#	vr = (0.005 + rand()* 0.0001)::Float64; #probability of speciation through cladogenesis
	vr = 0.0;# no cladogenesis 
	anaG = parse(Int,ARGS[2])::Int64;#Threshold to consider speciation
	mr = float(ARGS[1])::Float64;

	@time continentalecoevo.dynamic(seed,nreal,G,J,vr,mr,anaG,filename1,filename2,filename3);
end

main()


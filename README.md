# ContinentalEcoEvo


# Speciation events of clownfish in Coral reefs

Pellissier, De Santana and Melian (2014)

To Call: ./coralreef SEED NREAL G J FILE1 FILE2 FILE3

SEED  - (Random seed)

NREAL - (Number of realizations)

G     - (Number of generations per landscape)

J     - (Number of individuos per available site)

FILE1 - (Geographic Locations)

FILE2 - (File containing the names of the files of the Networks of Connections between sites)

FILE3 - (File containing the names of the files of the Matrices of dendritic distances between sites)

That means that each line of FILE2 and FILE3 represent one slice of the Dynamic-Landscape

Dynamic Landscape ------> t0 -> t0.5 -> t1.0 -> t1.5 -> ... -> tn

---------------------------------------------------------------


# step-by-step of the model

1) To perform the demographic dynamic during G/2 generations

2) To Compare matrix of distances in time t.0 and in time t.5
   a - For each site that disapear from t.0 to t.5, all individuals of this site will migrate to other sites in the same cluster/module/component (CONTRACTION event). The number of individuals that will migrate to each site will be proportional to the distance between these pair of sites. Probability of prevalence of the migrant individual (pp) is 50% (it means the prevalence is neutral)

   b - The model can be easily adapted to change the probability of prevalence: for niche prevalence, pp < 50%; for migrant prevalence, pp > 50%. 

3) To perform the demographic dynamic during other G/2 generations

4) To Compare matrix of distances in time t.5 and in time t+1.0
   a - For each site that apear from t.5 to t+1.0, individuals will migrate from all other sites in the same cluster/module/component (EXPANSION event). The number of individuals that will migrate to each site will be proportional to the distance between the source site and the target site. Probability of prevalence of the migrant individual (pp) is 100%. 

5) Come back to 1)


----------------------------------------------------------
- which mat1 - mat15 == 1

- To know which sites have disapeared from the landscape

- we call those sites as "contraction"

- for each site 'j' that belongs to "contraction"

- for all the individuals (Jj) alive in 'j'
 
- one site neighbor to 'j' (we call them 'n') will be chosen.

- then one random individual 'k' in 'n' will be chosen.

- probability of 0.5 to survive 'k' or 'Jj'

- which mat2 - mat15 == 1

- To know which new sites will appear in the landscape

- we call those sites as "expansion"
